---
weight: 10
title: Introduction
layout: bundle
---

This documentation describes the **10.6.0 release (April 2020)** of the Cumulocity IoT platform.

This documentation covers all new features and functionality introduced with release 10.6.0 (see the [Release notes](/release-notes#10.6.0) for details). 

Any other relevant changes that have been made to the Cumulocity IoT documentation since the previous version will be mentioned in the release notes as well.  

> **Important:** The **Installation and Operations guides** for the Cumulocity IoT platform are not publicly available. They are provided on the [Software AG Empower Portal](https://documentation.softwareag.com/). Search for "Cumulocity IoT Platform" as product. To view the Installation and Operations guides you need to click the PDF icon for the version of your choice.

![Documentation on Empower](/images/about/about-empower.png)


