---
weight: 30
title: Additional resources
layout: bundle
---

### Software AG TECHcommunity

You can find documentation and other technical information on the [Software AG TECHcommunity](http://techcommunity.softwareag.com/home/-/product/name/cumulocity) website. 

On this community, you can:

* Access articles, code samples, demos, and tutorials.
* Use the online discussion forums, moderated by Software AG professionals, to ask questions, discuss best practices, and learn how other customers are using Software AG technology.
* Link to external websites that discuss open standards and web technology.


### Software AG Empower Product Support Website

You can find product information on the [Software AG Empower Product Support](https://empower.softwareag.com) website.

>**Info**: If you do not have an account for Empower yet, send an email to <empower@softwareag.com> with your name, company, and company email address and request an account.

Once you have an account, you can: 

* Open Support Incidents online via the eService section of Empower. 
* Find further product information and product downloads.

To submit feature/enhancement requests, get information about product availability, and download products, go to [Products](https://empower.softwareag.com/Products/default.aspx?).

To get information about fixes and to read early warnings, technical papers, and knowledge base articles, go to the [Knowledge Center](https://empower.softwareag.com/KnowledgeCenter/default.aspx?).

If you have any questions, you can find a local or toll-free number for your country in our [Global Support Contact Directory](https://empower.softwareag.com/public_directory.asp) and give us a call.




