---
weight: 20
title: Documentation overview
layout: bundle
---

The Cumulocity IoT documentation on this website contains the following information:

<table>
<col width = 150>
<thead>
<tr>
<th align="left">Area</th>
<th align="left">Guide</th>
<th align="left">Content</th>
</tr>
</thead>

<tbody>
<tr>
<td align="left">User documentation</td>
<td align="left"><a href="/concepts/introduction/">Concept guide</a></td>
<td align="left">The architecture and technical concepts behind Cumulocity IoT. Intended for anyone technically interested in the machine-to-machine application platform.</td>
</tr>

<tr>
<td align="left">User documentation</td>
<td align="left"><a href="/users-guide/getting-started/">User guide</a></td>
<td align="left">Core, in-depth explanation on using the Administration, Cockpit and Device Management applications and various optional Cumulocity IoT services through the GUI.</td>
</tr>

<tr>
<td align="left">User documentation</td>
<td align="left"><a href="/device-tutorials/tutorials-introduction/">Device integration tutorials</a></td>
<td align="left">How to integrate various demo devices in your Cumulocity IoT environment.</td> 
</tr>

<tr>
<td align="left">User documentation</td>
<td align="left"><a href="/edge/overview/">Cumulocity IoT Edge</a></td>
<td align="left">How to install, run and operate Cumulocity IoT Edge, the local version of Cumulocity IoT.</td>
</tr>

<tr>
<td align="left">Developer documentation</td>
<td align="left"><a href="/reference/rest-implementation/">Reference guide</a></td>
<td align="left">Detailed technical specifications of the programming interfaces of Cumulocity IoT as a reference for software developers.</td>
</tr>

<tr>
<td align="left">Developer documentation</td>
<td align="left"><a href="/web/introduction/">Web SDK guide</a></td>
<td align="left">How to use the Web SDK to extend applications with your own plugins, add your own applications or implement further functionalities tailored to your use case.</td>
</tr>

<tr>
<td align="left">Developer documentation</td>
<td align="left"><a href="/microservice-sdk/introduction/">Microservice SDK guide</a></td>
<td align="left">How to develop and deploy microservices on top of Cumulocity IoT using the Microservice SDK.</td>
</tr>

<tr>
<td align="left">Developer documentation</td>
<td align="left"><a href="/device-sdk/introduction/">Device SDK guide</a></td>
<td align="left">How to integrate IoT data sources with the Cumulocity IoT platform.</td>
</tr>

<tr>
<td align="left">Analytics documentation</td>
<td align="left"><a href="/apama/overview-analytics/">Streaming Analytics guide</a></td>
<td align="left">Basics for understanding how to create your own analytics or other business logic in Cumulocity IoT with Apama EPL.</td>
</tr>

<tr>
<td align="left">Analytics documentation</td>
<td align="left"><a href="/predictive-analytics/introduction/">Machine learning guide</a></td>
<td align="left">Detailed information on the machine learning capabilities in the Cumulocity IoT platform.</td>
</tr>

<tr>
<td align="left">Analytics documentation</td>
<td align="left"><a href="/datahub/datahub-overview/">DataHub guide</a></td>
<td align="left">How to perform sophisticated analytical querying over device data, potentially covering long periods of time.</td>
</tr>
<tr>
<td align="left"><a href="/release-notes/overview/">Release Notes</a></td>
<td align="left"></td>
<td align="left">Find detailed information on new features, improvements and fixes implemented in each single Cumulocity IoT release, including maintenance releases.</td>
</tr>
</tbody>
</table>