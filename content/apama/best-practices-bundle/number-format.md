---
weight: 20
title: Number formats
layout: redirect
---

Cumulocity IoT measurements use the float type. Note that the timestamps are stored as floats (seconds since 1 Jan 1970, 00:00 UTC).