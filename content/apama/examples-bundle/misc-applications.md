---
weight: 50
title: Miscellaneous sample apps
layout: redirect
---
Apama EPL Apps provides several sample apps which demonstrate how to use Apama EPL, for example, to query for Cumulocity IoT objects or to create alarms. You can use these samples to build your own apps.
