---
weight: 20
title: Creating an EPL app
layout: redirect
---

Click the Apama EPL Apps icon in the application switcher to create a new EPL app. Select **New EPL app** to begin. You will now see an EPL editor window in which to create the app which interacts with the Zementis microservice.