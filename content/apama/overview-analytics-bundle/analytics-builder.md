---
weight: 30
title: Apama Analytics Builder
layout: redirect
---
Apama Analytics Builder is a web application which is available from the application switcher. It allows you to build analytic models that transform or analyze streaming data in order to generate new data or output events. The models are capable of processing data in real time.

You build the models in a graphical environment by combining pre-built *blocks* into *models*. The blocks in a model package up small bits of logic, and have a number of inputs, outputs and parameters. Each block implements a specific piece of functionality, such as receiving data from a sensor, performing a calculation, detecting a condition, or generating an output signal. You define the configuration of the blocks and connect the blocks using *wires*. You can edit the models, simulate deployment with historic data, or run them against live systems.

See the documentation for [Apama Analytics Builder for Cumulocity IoT](https://documentation.softwareag.com/onlinehelp/Rohan/Analytics_Builder/pab10-6-6/apama-pab-webhelp/index.html) for detailed information.

