---
weight: 20
title: Apama EPL Apps
layout: redirect
---
Apama EPL Apps is a web application which is available from the application switcher. It allows you to develop EPL apps (that is, single \*.mon files) directly within Cumulocity IoT, written in Apama EPL. You can also use it to import existing \*.mon files as EPL apps into Cumulocity IoT. When you activate an EPL app via the Apama EPL Apps web application, you deploy it to Cumulocity IoT.

See [Basic functionality](/apama/analytics-introduction/) for further information.
