---
weight: 20
title: Log files of the Apama-ctrl microservice
layout: redirect
---

There are two ways to get the logs of the Apama-ctrl microservice:

- Download diagnostics information as described in [Downloading diagnostics and logs](#diagnostics-download).
- In some cases, it is useful to view the log file of the Apama-ctrl microservice using Cumulocity IoT functionality. See [Managing applications](/users-guide/administration/#managing-applications) in the *User guide* for information on how to view log files.

Contact Software AG Support if needed.



