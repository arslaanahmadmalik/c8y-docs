---
weight: 50
title: Widgets-Sammlung
layout: redirect
---

<a name="widget"></a>

Die Cockpit-Anwendung enthält voreingestellte Widget-Typen. Jeder Widget-Typ ermöglicht es, verschiedene Parameter zu konfigurieren und verschiedene Daten anzuzeigen.

Im folgenden Abschnitt werden, in alphabetischer Reihenfolge, alle verfügbaren Widget-Typen und ihre Konfigurationsparameter beschrieben.

### Aktuelle Alarme

Das Widget "Aktuelle Alarme" zeigt alle Alarme aller Schweregrade, sortiert nach Zeit. Es können keinen zusätzlichen Parameter konfiguriert werden.

![Recent alarms widget](/images/benutzerhandbuch/cockpit/cockpit-widget-recent-alarms.png)

Nähere Informationen zu Alarmen finden Sie unter [Device Management > Verwenden von Alarmen](/benutzerhandbuch/device-management-de/#alarm-monitoring) im Abschnitt Device Management.

### Alarmliste

Das Widget "Alarmliste" zeigt eine Liste von Alarmen, gefiltert nach Objekten, Alarmschweregrad und Alarmstatus. Nähere Informationen zu Alarmen finden Sie unter [Device Management > Verwenden von Alarmen](/benutzerhandbuch/device-management-de/#alarm-monitoring).

![Alarm list widget](/images/benutzerhandbuch/cockpit/cockpit-widget-alarm-list.png)

**Konfigurierbare Parameter**

|Feld|Beschreibung|
|:---|:---|
|Titel|Widget-Titel. Standardmäßig wird der Widget-Typ als Titel verwendet.
|Ziel-Assets oder -geräte|Gruppen oder Geräte, optional HTML-Ausdrücke, die ausgewertet werden.
|Status|Alarmstatus, der angezeigt wird.
|Typ|Alarmtyp, der angezeigt wird. Klicken Sie auf einen Alarm, um Details anzuzeigen.
|Schweregrad|Alarmschweregrade, die angezeigt werden.
|Reihenfolge|Alarme können nach dem aktiven Status (gefolgt von Schweregrad und Zeit, Standardeinstellung) oder dem Schweregrad (gefolgt von der Zeit) sortiert werden.


### Alle kritischen Alarme

Das Widget "Alle kritischen Alarme" zeigt alle Objekte mit einem kritischen Alarm. Neben dem Titel können keine zusätzlichen Parameter konfiguriert werden.

![Critical alarms](/images/benutzerhandbuch/cockpit/cockpit-widget-critical-alarms.png)

Nähere Informationen zu Alarmen finden Sie unter [Device Management > Verwenden von Alarmen](/benutzerhandbuch/device-management-de/#alarm-monitoring) im Abschnitt Device Management.

### Ampel

Das "Ampel"-Widget visualisiert den Status eines Geräts im Form einer Ampel.

**Konfigurierbare Parameter**

|Feld|Beschreibung|
|:---|:---|
|Titel|Widget-Titel. Standardmäßig wird der Widget-Typ als Titel verwendet.
|Ziel-Assets oder -geräte|Objekt (Gruppe oder Gerät), das dargestellt wird.
|Statusregeln|Wählen Sie ein Attribut für jede Lampe. Wenn das Attribut einen der folgenden Werte hat, geht die entsprechende Lampe an: true, 1, jede nicht-leere Zeichenkette, jede Zahl außer 0.

### Anwendungen

Das Widget "Anwendungen" zeigt eine Liste mit Links zu allen verfügbaren Anwendungen. Neben dem Titel können keine zusätzlichen Parameter konfiguriert werden.

![Applications widget](/images/benutzerhandbuch/cockpit/cockpit-widget-applications.png)

Nähere Informationen zu Anwendungen finden Sie unter [Administration > Verwalten von Anwendungen](/benutzerhandbuch/administration-de#managing-applications).


### Asset-Anmerkungen

Das Widget "Asset-Nachrichten" zeigt Benachrichtigungen, die allen Besitzern des aktuellen Objekts vom Administrator bereitgestellt werden.

![Asset notes widget](/images/benutzerhandbuch/cockpit/cockpit-widget-asset-notes.png)

Nur Benutzer, die die Berechtigung haben, das Start-Dashboard zu bearbeiten, können solche Benachrichtigungen bereitstellen.


### Asset-Attribute

Das Widget "Asset-Attribute" zeigt eine benutzerdefinierte Liste von Attributen des aktuellen Objekts. Das aktuelle Objekt kann ein Gerät oder eine Gruppe sein.

![Asset properties widget](/images/benutzerhandbuch/cockpit/cockpit-widget-asset-properties.png)


**Konfigurierbare Parameter**

|Feld|Beschreibung|
|:---|:---|
|Titel|Widget-Titel. Standardmäßig wird der Widget-Typ als Titel verwendet.
|Ziel-Assets oder -geräte|Gruppen oder Geräte, die ausgewertet werden.
|Attribute|Liste von Attributen, siehe [Widget "Asset-Tabelle"](#widget-asset-table).

>**Info:** Im Ansichtsmodus zeigt diese Widget nur Attribute an, die nicht leer sind.

### <a name="widget-asset-table"></a> Asset-Tabelle

Das Widget "Asset-Tabelle" zeigt eine Tabelle mit Details zu den Kindgeräten an. Dies ist ein sehr mächtiges Widget, dass es ermöglicht, ausgewählte Attribute von Objekten in einer Tabelle zu arrangieren.

**Konfigurierbare Parameter**

|Feld|Beschreibung|
|:---|:---|
|Titel|Widget-Titel. Standardmäßig wird der Widget-Typ als Titel verwendet.
|Ziel-Assets oder -geräte|Objekte, für die die Kindgeräte angezeigt werden. Dies ist üblicherweise ein Gruppenobjekt.
|Attribute|Attribute oder Aktionen eines Objekts, die als Spalten in der Tabelle visualisiert werden.

**Beispiel**

Im folgenden Screenshot sind 5 Spalten konfiguriert. Die Spalten "Meter", “Vendor”, und “Owner” beziehen sich auf die Attribute “name”, type” und “owner”. Außerdem gibt es zwei Aktionen, eine für das Umschalten des Wartungsmodus und eine für das Neustarten des Systems.

![Asset table widget](/images/benutzerhandbuch/cockpit/cockpit-widget-asset-table.png)

Die daraus resultierende Tabelle wird folgendermaßen visualisiert:
![Asset table widget example](/images/benutzerhandbuch/cockpit/cockpit-widget-asset-table-example.png)

#### So fügen Sie Attribute hinzu

Klicken Sie **+Attribut hinzufügen** und wählen Sie ein oder mehrere Attribute aus.

> **Info:** Das Attribut "Anzahl aktiver Alarme" zeigt aktive Alarme als Symbole in der Tabelle. Wenn Sie dieses Attribut auswählen, müssen Sie den Renderer "Anzahl aktiver Alarme" in der Spaltenliste konfigurieren.

#### So fügen Sie Aktionen hinzu

1. Klicken Sie **+Kommando hinzufügen**.
1. Wählen Sie **Wartungsmodus umschalten** um die entsprechende vordefinierte Aktion hinzuzufügen.
1. Wählen Sie **Kommando erstellen**, um eine Schaltfläche zu erstellen, die ein Shell-Kommando ausführt. Im darauf folgenden Dialog können Sie eine Beschriftung für die Schaltfläche und das auszuführende Shell-Kommando eingeben.

![Reboot device button configuration](/images/benutzerhandbuch/cockpit/cockpit-widget-asset-table-buttonconfig.png)

>**Info:** Der Dialog zeigt die vordefinierten Shell-Kommandos des ersten Geräts, das Shell-Kommandos unterstützt. Gibt es kein solches Gerät, ist die Liste leer. Weitere Informationen finden Sie unter [Device Management > Shell-Kommandos](/benutzerhandbuch/device-management-de/#shell).<br>
Sie können auch das JSON-Format für das Kommando eingeben, das zum Gerät gesendet wird. Fragen Sie den Gerätehersteller nach unterstützten Kommandos, um weitere Details zu erfahren.

#### So ändern Sie die Tabelle

Um die Kopfzeile einer Spalte zu bearbeiten, klicken Sie auf den Wert in der Spalte **Beschriftung** und bearbeiten Sie die Beschriftung.

Sie können die Spalten umsortieren, in dem Sie auf das Symbol ganz links in einer Zeile klicken und den Eintrag durch Ziehen und Ablegen verschieben.

Um ein Attribut oder eine Aktion zu löschen, fahren Sie mit dem Mauszeiger über die entsprechende Zeile und klicken Sie **Löschen** auf der rechten Seite.

### Bild

Das Widget "Bild" ermöglicht es, ein einzelnes Bild anzuzeigen, das Sie aus Ihrem Dateisystem hochladen können. Es können keinen zusätzlichen Parameter konfiguriert werden.

### Datenpunktgraph

Das Widget "Datenpunktgraph" zeigt einen Datenpunkt (Messwert) in einem Graphen. Die Visualisierung ist identisch mit der im [Daten-Explorer](/benutzerhandbuch/cockpit-de/#data-explorer).

<img src="/images/benutzerhandbuch/cockpit/cockpit-datapointsgraph-widget.png" name="Data Point Graph widget" style="width:75%;"/>

Am einfachsten erstellen Sie ein "Datenpunktgraph"-Widget, in dem Sie zum Daten-Explorer navigieren, **Mehr...** in der oberen Menüleiste klicken und **Als Widget zum Dashboard senden** wählen.

Weitere Informationen zu den konfigurierbaren Parametern finden Sie unter [Visualisieren von Daten mit dem Daten-Explorer](/benutzerhandbuch/cockpit-de/#data-explorer).

### Datenpunktliste

Das Widget "Datenpunktliste" zeigt Datenpunkte (Messwerte) in einer Liste, einen pro Zeile, mit aktuellen Werten und Datenpunktattributen.

**Konfigurierbare Parameter**

<table>
<thead>
<colgroup>
   <col style="width: 20%;">
   <col style="width: 80%;">
</colgroup>
<tr>
<th align="left">Feld</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left">Titel</td>
<td align="left">Widget-Titel. Standardmäßig wird der Widget-Typ als Titel verwendet.</td>
</tr>
<tr>
<td align="left">Datenpunkte</td>
<td align="left">Zeigt eine Liste verfügbarer Datenpunkte. Sie müssen mindestens einen Datenpunkt aktivieren. Klicken Sie <strong>Datenpunkt hinzufügen</strong>, um einen Datenpunkt zur Liste hinzuzufügen. Informationen zum Hinzufügen von Datenpunkten finden Sie unter <a href="#add-data-points">Daten-Explorer &gt; Hinzufügen von Datenpunkten</a>.</td>
</tr>
<tr>
<td align="left">Sichtbare Tabellenspalten</td>
<td align="left">Spalten, die angezeigt werden: <br><strong>Beschriftung</strong>: Beschriftung des Datenpunkts. Details finden Sie unter <a href="../../benutzerhandbuch/cockpit-de/#data-explorer">Visualisieren von Daten im Daten-Explorer</a>. <br><strong>Ziel</strong>: Zielwert. Kann im <a href="../../benutzerhandbuch/cockpit-de/#data-explorer">Daten-Explorer</a> oder in der <a href="../../benutzerhandbuch/cockpit-de/#data-point-library">Datenpunktbibliothek</a> konfiguriert werden.<br>Aktuell: Aktueller Wert. <br><strong>Differenz</strong>: Absolute Differenz zwischen aktuellem Wert und Zielwert. <br><strong>Differenz %</strong>: Prozentwert der Differenz zwischen aktuellem Wert und Zielwert. <br><strong>Asset</strong>: Name des Geräts oder der Gruppe des Datenpunkts.</td>
</tr>
</tbody>
</table>

### Datenpunkttabelle

Die Konfiguration des Widgets "Datenpunkttabelle" ist ähnlich wie die des Widgets "Datenpunktgraph". Die Daten werden jedoch nicht als Linien, sondern als Tabelle dargestellt.

Das Widget "Datenpunkttabelle" zeigt Daten basierend auf ausgewählten Datenpunkten, einem Zeitintervall und Aggregation.

Werte außerhalb eines bestimmten Bereichs, basierend auf konfigurierten gelben und roten Bereichen, werden in der Tabelle hervorgehoben.

![Data point table](/images/benutzerhandbuch/cockpit/cockpit-datapointtable.png)

### Ereignisliste

Das Widget "Ereignisliste" ermöglicht es, Ereignisse für ein ausgewähltes Gerät zu überwachen.

![Event list widget](/images/benutzerhandbuch/cockpit/cockpit-widget-event-list.png)

Außerdem kann ein Zeitintervall festgelegt und Ereignisse können in Echtzeit überwacht werden.

### Fieldbus-Gerät

Das Widget "Fieldbus-Gerät" ermöglicht es, den Status eines Modbus-Geräts anzuzeigen und dieses zu betreiben.

Weitere Informationen zum Widget "Fieldbus-Gerät" finden Sie unter [Optionale Services > Cloud Fieldbus > Monitoring device status using the Fieldbus device widget](/users-guide/optional-services#fieldbus-device-widget).

### Gerätenachricht

Das Widget "Gerätenachricht" sendet eine Nachricht an ein Gerät. Das Verhalten des Geräts selbst ist geräteunabhängig. Nur verfügbar für Geräte, die diese Art von Kommando unterstützen.

### Hilfe und Service

Das Widget "Hilfe und Service" zeigt Links zu Hilfe- und Serviceangeboten. Es können keinen zusätzlichen Parameter konfiguriert werden.

![Help and service widget](/images/benutzerhandbuch/cockpit/cockpit-widget-help-service.png)

### HTML

Das Widget "HTML" zeigt benutzerdefinierten Inhalt. Die Inhalt kann mit HTML formatiert werden.

**Konfigurierbare Parameter**

* Ziel-Assets oder -geräte: Wählen Sie die Objekte aus, für die optionale HTML-Ausdrücke ausgewertet werden sollen.

* HTML-Code

	Die folgenden Variablen können im HTML-Code verwendet werden:

	* {{devicesCount}}: Gesamtanzahl der Geräte.

	* {{usersCount}}: Gesamtanzahl der Benutzer.

	* {{deviceGroupsCount}}: Gesamtanzahl der Gruppen.

	* {{device.name}}: Name des Geräts.

	* {{device.*property*}}: Allgemeinere Form des oben genannten. Sie können jedes Attribut des Geräts ansprechen.

	* {{device.c8y_Hardware.model}}: Modell des Geräts.

	* {{device.*fragment*.*property*}}: Allgemeinere Form des oben genannten. Sie können jedes Attribut oder Fragment des Geräts ansprechen.

"device" bezieht sich auf das im Widget-Konfigurationsparameter ausgewählte Zielgerät.<br>
"fragment.property" bezieht sich auf die Attribute des betreffenden Geräts. Um die verfügbaren Attributnamen anzuzeigen, können Sie in der Konfiguration des Widgets "Asset-Attribut" oder “Asset-Tabelle” auf **+Attribut hinzufügen** klicken. Daraufhin wird eine Tabelle der unterstützten Attribute angezeigt. Sie können die Werte aus der Spalte **Attribut** kopieren und einfügen. Generierte Attribute dieser Widgets sind nicht in den HTML-Widgets verfügbar.

![HTML widget](/images/benutzerhandbuch/cockpit/cockpit-widget-html.png)

### Infoanzeige

Das Widget "Infoanzeige" visualisiert einen Datenpunkt in Form eines Tachos und mehrere Datenpunkte als Beschriftung.

![Info gauge widget](/images/benutzerhandbuch/cockpit/cockpit-widget-info-gauge.png)

Sie können einen Datenpunkt für das Tacho auswählen sowie mehrere Datenpunkte, die als Beschriftung auf der linken Seite angezeigt werden.

![Info gauge widget data point gauge](/images/benutzerhandbuch/cockpit/cockpit-widget-data-gauge.png)

![Info gauge widget data point label](/images/benutzerhandbuch/cockpit/cockpit-widget-data-labels.png)

Sie müssen mindestens einen Datenpunkt in jedem Bereich aktivieren, um das Widget "Infoanzeige" zu erstellen.

### Karte

Das Widget "Karte" zeigt den Standort eines Geräts oder aller Geräte einer Gruppe an.

![Info gauge widget](/images/benutzerhandbuch/cockpit/cockpit-widget-map.png)

Sie können den Inhalt der Karte mit dem Mauszeiger verschieben sowie mit den Schaltflächen **Plus** und **Minus** rein- bzw. rauszoomen.

Die Symbole, die die Geräte repräsentieren, sind farbkodiert. Die Farben werden nach folgenden Regeln verwendet:

* Rot = Mindestens ein KRITISCHER Alarm
* Orange = Mindestens ein WICHTIGER Alarm
* Gelb = Mindestens ein WENIGER WICHTIGER Alarm
* Blau = Mindestens eine WARNUNG
* Grün = Keine Alarme

Klicken Sie auf ein Gerätesymbol, um folgende Details in einem Popup-Fenster anzuzeigen:

* Name des Geräts. Klicken Sie darauf, um zu diesem Gerät zu navigieren.
* Datum, an welchem das Gerät zuletzt seinen Standort gesendet hat, wenn verfügbar.
* Umschalter, um die Geräte-Tracks für die vergangenen und zukünftigen Tage anzuzeigen bzw. zu verbergen.

**Konfigurierbare Parameter**

Ziel-Assets oder -geräte: Geräte, die auf der Karte angezeigt werden. Im Falle einer Gruppe werden alle Geräte in dieser Gruppe (aber nicht in Untergruppen) angezeigt.

>**Info:** Wenn keines der Zielgeräte einen bekannten Standort hat, zeigt das Widget eine Weltkarte ohne Symbol.

### Kuchendiagramm

Das Widget "Kuchendiagramm" zeigt Datenpunkte (Messwerte) mit aktuellen Werten in einem Kuchendiagramm.

**Konfigurierbare Parameter**

<table>
<thead>
<colgroup>
   <col style="width: 20%;">
   <col style="width: 80%;">
</colgroup>
<tr>
<th align="left">Feld</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left">Titel</td>
<td align="left">Widget-Titel. Standardmäßig wird der Widget-Typ als Titel verwendet.</td>
</tr>
<tr>
<td align="left">Kuchendiagramm-Optionen</td>
<td align="left">Auswahloptionen zum Anzeigen von Tooltips, Prozentwerten und Legenden im Kuchendiagramm.</td>
</tr>
<tr>
<td align="left">Datenpunkte</td>
<td align="left">Zeigt eine Liste verfügbarer Datenpunkte. Sie müssen mindestens einen Datenpunkt aktivieren. Klicken Sie <strong>Datenpunkt hinzufügen</strong>, um einen Datenpunkt zur Liste hinzuzufügen. Informationen zum Hinzufügen von Datenpunkten finden Sie unter <a href="#add-data-points">Daten-Explorer &gt; Hinzufügen von Datenpunkten</a>.</td>
</tr>
</tbody>
</table>

### Linearer Zeiger

Das Widget "Linearer Zeiger" visualisiert Datenpunkte in Form eines linearen Messgeräts. Minimale und maximale Zielwerte werden ebenfalls angezeigt.

![Info gauge widget](/images/benutzerhandbuch/cockpit/cockpit-widget-linear-gauge.png)

>**Info:** Wenn eine Beschriftung nicht vollständig angezeigt werden kann, können Sie sich damit behelfen, den minimalen und maximalen Wert zu erhöhen und so die Beschriftung in den lesbaren Bereich zu verschieben.

Sie müssen mindestens einen Datenpunkt aktivieren, um das Widget "Linearer Zeiger" zu erstellen.

### Quick Links

Das Widget "Quick links" zeigt verschiedene Links für den schnellen Zugriff auf relevante Kommandos an. Es können keinen zusätzlichen Parameter konfiguriert werden.

![Quick links widget](/images/benutzerhandbuch/cockpit/cockpit-widget-quick-links.png)

### Relaisfeldsteuerung

Das Widget "Relaisfeldsteuerung" ermöglicht es, Relais in einem Relaisfeld unabhängig voneinander an- oder auszuschalten. Nur verfügbar für Geräte, die diese Art von Kommando unterstützen.

### Relaissteuerung

Das Widget "Relaissteuerung" ermöglicht es, ein Geräterelais an- oder auszuschalten. Nur verfügbar für Geräte, die diese Art von Kommando unterstützen.

### Rotationsmodell

Das Widget "Rotationsmodell" ermöglicht es, ein Objektmodell eines Geräts zu rendern.

**Konfigurierbare Parameter**

|Feld|Beschreibung|
|:---|:---|
|Titel|Widget-Titel. Standardmäßig wird der Widget-Typ als Titel verwendet.
|Ziel-Assets oder -geräte|Objekt (Gruppe oder Gerät), das dargestellt wird.
|Objektmodell für das Rendering|Wählen Sie einen Objektmodelltyp für das Rendering. Modelltyp für das Rendering, entweder "Box" oder "Telefon".
|Gitternetz|Die Option "Gitternetz" kann an- oder abgeschaltet werden (Standardeinstellung = an). Der "Gitternetz"-Modus stellt das Objekt in einer netzartigen Repräsentation dar.
|Kameratyp|Wählen Sie den zu verwendenden Kameratyp. Zur Auswahl stehen die Optionen "Orthografische Kamera" und "Perspektivische Kamera".

Im Rotation-Widget kann das Objekt durch Ziehen und Bewegen gedreht werden. Außerdem kann mit der Maus rein- und rausgezoomt werden.

### SCADA

Das Widget "SCADA" bietet eine graphische Darstellung eines Gerätestatus.

Nähere Informationen zum Widget "SCADA" finden Sie unter [Optionale Services > Cloud Fieldbus > Monitoring status using the SCADA widget](/users-guide/optional-services/#scada).

> **Info:** Alle SVG-Dateien werden bereinigt, um schädlichen Code zu entfernen.

![SCADA widget](/images/benutzerhandbuch/cockpit/cockpit-widget-scada.png)

### Silo

Das "Silo"-Widget zeigt Datenpunkte (Messwerte) mit aktuellen Werten in einer Silo-Darstellung an.

**Konfigurierbare Parameter**

<table>
<thead>
<colgroup>
   <col style="width: 20%;">
   <col style="width: 80%;">
</colgroup>
<tr>
<th align="left">Feld</th>
<th align="left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td align="left">Titel</td>
<td align="left">Widget-Titel. Standardmäßig wird der Widget-Typ als Titel verwendet.</td>
</tr>
<tr>
<td align="left">Datenpunkte</td>
<td align="left">Zeigt eine Liste verfügbarer Datenpunkte. Sie müssen mindestens einen Datenpunkt aktivieren. Klicken Sie <strong>Datenpunkt hinzufügen</strong>, um einen Datenpunkt zur Liste hinzuzufügen. Informationen zum Hinzufügen von Datenpunkten finden Sie unter <a href="#add-data-points">Daten-Explorer &gt; Hinzufügen von Datenpunkten</a>.</td>
</tr>
</tbody>
</table>

### Tacho

Das Widget "Tacho" visualisiert Datenpunkte in Form eines Tachos.

![Radial gauge widget](/images/benutzerhandbuch/cockpit/cockpit-widget-radial-gauge.png)

Sie müssen mindestens einen Datenpunkt aktivieren, um das Widget "Tacho" zu erstellen.
