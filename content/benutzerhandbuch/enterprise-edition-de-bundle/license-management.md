---
weight: 90
title: Lizenzverwaltung
layout: redirect
---

Der Management-Mandant kann die Lizenzen für die Domains der Enterprise Tenants über die Benutzeroberfläche verwalten.

![License management](/images/benutzerhandbuch/enterprise-tenant/et-license-management.png)

### So können Sie eine Domain-Lizenz hinzufügen und validieren

1. Melden Sie sich beim Management-Mandanten an.
2. Navigieren Sie zur Seite **Lizenzverwaltung** im Menü **Einstellungen**. 
Auf der Seite **Lizenzverwaltung** werden alle Domain-Lizenzen zusammen mit ihrem Status aufgelistet (grünes Häkchen = gültig).
1. Fügen Sie den Lizenzschlüssel in das Feld **Mandantenlizenz hinzufügen** ein und klicken Sie **Hinzufügen**.

Die Lizenz wird validiert und den Mandantenoptionen hinzugefügt.

### So löschen Sie eine Domain-Lizenz

1. Melden Sie sich beim Management-Mandanten an.
2. Navigieren Sie zur Seite **Lizenzverwaltung** im Menü **Einstellungen**. 
3. Klicken Sie auf das Menüsymbol rechts neben der Domain, für die Sie die Lizenz löschen möchten, und klicken Sie dann **Löschen**. 