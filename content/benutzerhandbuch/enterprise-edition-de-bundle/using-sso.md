---
weight: 70
title: Verwenden von Single-Sign-On (SSO)
layout: redirect
---

Um die Single-Sign-On-Funktion für Enterprise Tenants nutzen zu können, muss die Unternehmens-Domain in den Grundeinstellungen als Redirect-URI festgelegt sein.
Weitere Informationen finden Sie unter [Ändern von Einstellungen > Konfigurieren von Single-Sign-On](/benutzerhandbuch/administration-de#single-sign-on) unter "Administration".

Wenn Single-Sign-On-Anbieter über Whitelists verfügen, sollte die Unternehmens-Domain auf der Whitelist geführt werden.