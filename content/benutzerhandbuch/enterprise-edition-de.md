---
weight: 50
title: Enterprise Tenant
layout: bundle
---

Der Enterprise Tenant der Cumulocity IoT-Plattform bietet verschiedene Erweiterungen zu den Funktionalitäten des Standard Tenant. Im Folgenden sind zusätzliche Funktionalitäten beschrieben, die nur im Enterprise Tenant verfügbar sind.