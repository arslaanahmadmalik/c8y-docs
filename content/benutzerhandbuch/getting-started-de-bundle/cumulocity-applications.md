---
weight: 30
title: Cumulocity IoT-Anwendungen
layout: redirect
aliases:
  - /benutzerhandbuch/overview/#cumulocity-applications
---

Cumulocity IoT umfasst die drei folgenden Standardanwendungen:

<table>
<col width = 100>
<col width = 150>
<thead>
<tr>
<th style="text-align:center">&nbsp;</th>
<th style="text-align:left">Anwendung</th>
<th style="text-align:left">Beschreibung</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center"><i class="c8y-icon c8y-icon-administration c8y-icon-duocolor" style="font-size: 36px;"></i></td>
<td style="text-align:left"><a href="/benutzerhandbuch/administration-de" class="no-ajaxy">Administration</a></td>
<td style="text-align:left">Die "Administration"-Anwendung ermöglicht es Administratoren, ihre Benutzer, Rollen, Mandanten, Anwendungen und Regeln zu verwalten sowie eine Reihe von Einstellungen für ihr Konto zu konfigurieren. </td>
</tr>
<tr>
<td style="text-align:center"><i class="c8y-icon c8y-icon-cockpit c8y-icon-duocolor" style="font-size: 36px;"></i></td>
<td style="text-align:left"><a href="/benutzerhandbuch/cockpit-de" class="no-ajaxy">Cockpit</a></td>
<td style="text-align:left">Die Cockpit-Anwendung umfasst Optionen für die Verwaltung und Überwachung von IoT-Assets und Daten aus Geschäftssicht.</td>
</tr>
<tr>
<td style="text-align:center"><i class="c8y-icon c8y-icon-device-management c8y-icon-duocolor" style="font-size: 36px;"></i></td>
<td style="text-align:left"><a href="/benutzerhandbuch/device-management-de" class="no-ajaxy">Device Management</a></td>
<td style="text-align:left">Die Device Management-Anwendung bietet Funktionalitäten zur Verwaltung und Überwachung von Geräten und ermöglicht es, per Fernzugriff Geräte zu steuern und Fehler zu beheben.  </td>
</tr>

</tbody>
</table>

Neben diesen in Cumulocity IoT enthaltenen Standardwendungen gibt es zahlreiche weitere Anwendungen, die sie abonnieren können, wie etwa integrierte Agenten für verschiedene Gerätetypen. Die Verfügbarkeit dieser Anwendungen hängt davon ab, welchen Mandanten Sie verwenden und welche Anwendungen Ihr Unternehmen abonniert hat.

Eine ausführliche Liste aller mit der Cumulocity IoT-Plattform verfügbaren Anwendungen finden Sie unter [Administration > Verwalten von Anwendungen](/benutzerhandbuch/administration-de#managing-applications).

Darüber hinaus bietet die Cumulocity IoT-Plattform vielfältige Funktionalitäten zur Nutzung Ihrer eigenen Anwendungen über Ihr Cumulocity IoT-Konto, siehe Details unter [Administration > Verwalten von Anwendungen](/benutzerhandbuch/administration-de#managing-applications).

Der Inhalt der Cumulocity IoT-Plattform ist also vollständig dynamisch und wird anhand der folgenden Kriterien generiert:

* Anwendungen, die Ihr Unternehmen abonniert hat.
* Anwendungen, die Ihr Unternehmen selbst für Cumulocity IoT entwickelt oder konfiguriert hat.
* Funktionalitäten, die Ihr Benutzer sehen kann.
* Konfiguration der Benutzeroberfläche, wie etwa Gruppen oder Berichte.
