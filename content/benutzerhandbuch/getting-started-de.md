---
title: Erste Schritte
weight: 10
layout: bundle
aliases:
  - /benutzerhandbuch/overview
---
Der Abschnitt *Erste Schritte* beschreibt, wie Sie die Cumulocity IoT-Plattform aufrufen und sich anmelden, und erklärt die wesentlichen Funktionen, die in allen Anwendungen auf der Plattform identisch sind.
