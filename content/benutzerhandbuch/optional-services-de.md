---
weight: 60
title: Optionale Services
layout: bundle
slug: optional-services
---

Zusätzlich zu den in Cumulocity IoT verfügbaren integrierten Anwendungen für den Standard Tenant und den Enterprise Tenant gibt es zahlreiche optionale Services, die Sie abonnieren können, wie etwa Agenten für verschiedene Gerätetypen.

Die folgenden Services sind aktuell verfügbar:

|Service|Beschreibung|
|:---|:---|
|[Cloud Fieldbus](/users-guide/optional-services/#cloud-fieldbus)|Ermöglicht es, Daten von Fieldbus-Geräten zu empfangen und diese fernzusteuern.
|[Cloud Sensor App](/users-guide/optional-services/#android-cloud-sensor-app)|Verbindet Android Smartphones, iOS Smartphones oder TI Sensor Tags mit Cumulocity, um Messwerte von diesen Geräten zu verarbeiten oder diese fernzusteuern.
|[Cloud Remote Access](/users-guide/optional-services/#cloud-remote-access)|Implementiert Virtual Network Computing (VNC), um über einen Web-Browser auf Steuerelemente und andere Geräte zuzugreifen.
|[Connectivity](/users-guide/optional-services/#connectivity)|Stellt Basisinformationen zu mobilen Geräten und weitere Details zur Konnektivität bereit.
|[IMPACT](/users-guide/optional-services/#nokia-impact)|Ermöglicht die Integration von Nokia IMPACT in die Cumulocity-Plattform, um Daten von heterogenen Geräten zu verarbeiten.
|[LoRa Actility](/users-guide/optional-services/#lora)|Ermöglicht die Kommunikation mit LoRa-Geräten über Actility's ThingPark Wireless.
|[LightweightM2M](/users-guide/optional-services/#lwm2m)|Ermöglicht die Kommunikation mit allen Geräten, die das LWM2M-Protokoll unterstützen.
|[OPC UA](/users-guide/optional-services/#opc-ua)|Ermöglicht die Kommunikation zwischen der Cumulocity IoT-Plattform und einem OPC-UA-Server über ein OPC-UA-Gateway.
|[Sigfox](/users-guide/optional-services/#sigfox)|Ermöglicht die Kommunikation mit Sigfox-Geräten über die Sigfox-Cloud.
|[SNMP](/users-guide/optional-services/#snmp)|Ermöglicht die Kommunikation mit SNMP-fähigen Geräten.


<br>

> **Info**: Die Dokumentation der optionalen Services liegt ausschließlich in englischer Sprache vor.
