---
weight: 40
title: Developing applications
layout: bundle
aliases:
  - /concepts-guide/developing-m2m-applications
  - /concepts-guide/developing-m2m-applications.html
  - /concepts-guide/hosting
  - /concepts-guide/hosting.html
  - /konzepte/anwendungen
---

Cumulocity IoT is designed to accommodate arbitrary vertical IoT applications in addition to its generic functionality.

This section introduces the basic concepts around applications in Cumulocity IoT.