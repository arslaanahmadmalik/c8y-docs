---
weight: 50
title: How does Cumulocity IoT support developing agents?
layout: redirect
---


Cumulocity IoT supports agent development on three different levels:

* There are a number of full-featured open source agents and drivers in Cumulocity's [bitbucket.org](https://bitbucket.org/m2m/cumulocity-examples) and [mbed.org](http://mbed.org/users/vwochnik/code/) repositories. More information can be found in the "Devices" section of this documentation.
* Client libraries for major runtime environments such as C/C++, JavaME/SE and Lua, again as open source in [bitbucket.org](https://bitbucket.org/m2m).
* Technology-neutral [REST APIs](/device-sdk/rest#device-integration) for other runtime environments.


