---
title: APIs
weight: 70
---

Cumulocity IoT exposes its complete functionality through programming interfaces (APIs). This means that all of Cumulocity IoT's functionality is available for you to use in different contexts outside of what Cumulocity IoT directly provides - in your own applications, in your own devices.

In contrast to many other M2M and IoT platforms, Cumulocity IoT uses the same APIs and the same interface technology for all use cases. As a consequence, you have a wider range of choices in putting intelligence into your IoT devices, depending on how powerful they are. You also have to use only one set of APIs and one technology to build a complete solution from device to application on your own.

Cumulocity IoT uses HTTP and REST, which is today the most widely used interfacing technology and which works on any internet-connected device ranging from small embedded microcontrollers up to desktop PCs. The secure variant, HTTPS, is used for the most security critical applications and will give you the best possible security.

The plugin concept of Cumulocity IoT enables you to write new user interface functionality that will seamlessly extend the existing Cumulocity IoT platform.