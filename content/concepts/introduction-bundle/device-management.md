---
title: Device management
weight: 40
---

Cumulocity IoT provides extensive device management for fully certified devices. This includes

* Hardware and modem information.
* Connection monitoring.
* Centralized fault management and service level monitoring.
* Configuration management.
* Software and firmware management.
* Graphs of device statistics.
* Frequently used remote controls (for example, restart button, switches).
* Troubleshooting features such as events list and operations queue.

The level of depth in device management may depend on device features (for example, if a device does not support remote firmware upgrade, it will also not be available through Cumulocity IoT). For interfacing devices not yet certified with Cumulocity IoT, the [Device management library](/reference/device-management) and the [Device SDK guide](/device-sdk/rest#device-integration) is publicly available.

![Device Management](/images/users-guide/DeviceManagement/devmgmt-devices-info.png)

