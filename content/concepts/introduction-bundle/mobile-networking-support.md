---
title: Mobile networking support
weight: 30
---

Cumulocity IoT supports any type of internet connectivity in a secure manner. It gracefully deals with intermittent, bandwidth-restricted and uni-directional connections (such as communication through NAT). Where desired, Cumulocity IoT can control remote devices in a real-time manner.

Mobile internet connectivity is an ideal choice for many machine-to-machine applications, since it works well nearly everywhere without requiring any integration with a company's network infrastructure. This is especially true if your M2M SIM card allows for free roaming between mobile networks. The large bandwidths requirements of consumer applications are often not required. With Cumulocity IoT, you can benefit from mobile connectivity without requiring additional network provider services such as VPNs and public or even static IP addressing.