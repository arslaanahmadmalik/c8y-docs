---
weight: 80
title: Summary

---

Cumulocity IoT is an independent device and application management IoT platform. It connects and manages your devices and assets efficiently and can control them remotely.

* Connect your devices and assets over any network.
* Monitor conditions and generate real-time analytics.
* React immediately to conditions or situations.