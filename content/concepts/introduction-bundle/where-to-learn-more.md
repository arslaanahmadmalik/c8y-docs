---
title: Where to learn more
weight: 70
---

More conceptual information can be found in the following sections of the Concept guide:

* The general technical concepts behind Cumulocity IoT are described in [Cumulocity IoT's domain model](/concepts/domain-model).
* Concepts related to interfacing devices and other IT systems with Cumulocity IoT are described in [Interfacing devices](/concepts/interfacing-devices).
* Customization concepts of Cumulocity IoT are described in [Real-time processing](/concepts/realtime) and [Developing applications](/concepts/applications).
* Security concepts are described in [Security aspects](/concepts/security).