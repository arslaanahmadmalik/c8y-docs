---
weight: 10
title: Introduction to Cumulocity IoT
layout: bundle
aliases:
  - /concepts-guide/introduction-to-cumulocity
  - /concepts-guide/introduction-to-cumulocity.html
  - /concepts/acl
  - /konzepte/einfuehrung
  - /konzepte/introduction
---

Cumulocity IoT gives you very fast visibility and control over your remote assets, be these houses, cars, machines or any other assets that you want to manage.