---
weight: 35
title: Real-time processing
layout: bundle
aliases:
  - /concepts-guide/real-time-processing-in-cumulocity
  - /concepts-guide/real-time-processing-in-cumulocity.html
  - /konzepte/echtzeit
---

Cumulocity IoT allows developers and power users to run real-time IoT business logic inside Cumulocity IoT based on a high-level real-time processing language. 

This section introduces the basic concepts of real-time processing and shows how you can develop your own functional business logic at Cumulocity IoT.