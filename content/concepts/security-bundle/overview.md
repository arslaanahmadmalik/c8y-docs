---
weight: 10
title: Overview
layout: redirect
---

This section will show security concepts and aspects of Cumulocity IoT, structured into physical security, network security, application security and access control. Finally, it shows how Cumulocity IoT helps in managing the security of your IoT solution. 

This section is especially intended for IT security staff and management staff. IT security expertise is required when running Cumulocity IoT.

More information can be found in the security-related sections of the remaining documentation, like the [REST implementation](/reference/rest-implementation) and [User API](/reference/users) description in the Reference guide. Permissions required for individual API calls are documented in the respective Reference guide sections for the APIs.