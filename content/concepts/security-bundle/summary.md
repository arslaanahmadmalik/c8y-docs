---
weight: 70
title: Summary
layout: redirect
---

Cumulocity IoT addresses security on various levels.

All business partners and service providers have recognized security certificates. Cumulocity IoT also deals with network security aspects by individual authentication and authorization methods. 

Connections from and to Cumulocity IoT are established using HTTPS technology.

All tenants have full rights to add or terminate users and user groups. The tenant also assigns rights to agents and devices.

