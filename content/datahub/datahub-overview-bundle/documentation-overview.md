---
weight: 10
title: Documentation overview
layout: redirect
---

The following sections will walk you through all the functionalities of Cumulocity IoT DataHub in detail.

For your convenience, here is an overview of the contents of this document:

| Section | Content |
| -----   | -----   |
| [Getting started](/datahub/getting-started-with-datahub) | Log into DataHub and get an overview of the UI features |
| [Setting up DataHub](/datahub/setting-up-datahub) | Set up DataHub and its components |
| [Working with DataHub](/datahub/working-with-datahub) | Manage offloading pipelines and query the offloaded results |
| [Operating DataHub](/datahub/operating-datahub) | Run administrative tasks |
| [Running DataHub on the Edge](/datahub/running-datahub-on-the-edge) | Run administrative tasks |