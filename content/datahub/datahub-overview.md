---
title: DataHub overview
weight: 10
layout: bundle
---

This section outlines the structure of the document and gives a high-level introduction into Cumulocity IoT DataHub (CDH) and its concepts.

