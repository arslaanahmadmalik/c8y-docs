---
weight: 10
title: Accessing and logging into DataHub
layout: redirect
---

You access Cumulocity IoT DataHub via a web browser. It has been tested with the following web browsers:

* Firefox (latest version)
* Chrome (latest version)

> **Info:** Mobile devices like smartphones or tablets are not supported.

### How to log into DataHub

After the Cumulocity IoT DataHub services have been subscribed for your tenant, you have to log into your tenant. In the **application switcher** you will find the DataHub button. When you click this button, you will be taken to the home page of DataHub. DataHub provides the management and monitoring UI of Cumulocity IoT DataHub. Alternatively your tenant administrator can give you the URL directly navigating to DataHub, which requires a login as well.

When you want to log out, click the **User** button at the right of the top bar and from the context menu select **Logout**.
