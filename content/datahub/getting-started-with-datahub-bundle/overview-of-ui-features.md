---
weight: 20
title: Overview of UI features
layout: redirect
---

DataHub provides the UI you use for managing and monitoring your offloading pipelines. The main navigation bar on the left provides links to the relevant pages. The access to these pages is restricted and depends on corresponding user roles/permissions as defined in section [Defining DataHub permissions and roles](/datahub/setting-up-datahub#defining-permissions).

| Page | Description | Required role
| ---  | --- | ---
| Home | Get an introduction to DataHub and access quick links with related functionality | -
| Settings | Set up DataHub | DATAHUB_ADMINISTRATOR
| Offloading | Configure and manage your offloading pipelines | DATAHUB_ADMINISTRATOR or DATAHUB_MANAGER
| Status | View the latest job status of your offloading pipelines | DATAHUB_ADMINISTRATOR or DATAHUB_MANAGER
| Auditing | View the audit log | DATAHUB_ADMINISTRATOR
| Administration | Check system status | DATAHUB_ADMINISTRATOR

<img src="/images/datahub-guide/datahub-home-page.png" alt="DataHub homepage"  style="max-width: 100%">