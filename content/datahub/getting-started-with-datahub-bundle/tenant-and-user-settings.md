---
weight: 30
title: Tenant and user settings
layout: redirect
---

The DataHub instance you have access to is tenant-specific, i.e., this instance solely serves your tenant. You use your Cumulocity IoT account to access your DataHub instance.

In addition to your Cumulocity IoT account, you have a separate account (which is created during the initial setup) for accessing Dremio. Contact your administrator for the Dremio account credentials. On the **Home** page there is a direct link to the Dremio instance under **Quick links** .
