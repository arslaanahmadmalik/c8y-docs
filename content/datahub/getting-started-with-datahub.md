---
title: Getting started with DataHub
weight: 20
layout: bundle
---

The _Getting started_ section describes how to access Cumulocity IoT DataHub and walks you through the main UI features.


