---
weight: 20
title: Viewing audit logs
layout: redirect
---

Audit logs show the operations that users have carried out.

In the navigator, select **Auditing** to view the audit log list. For each log entry, the following information is provided:

| Column name | Description
| ---         |  ---
| User | The user that has carried out the operation.
| Event | The type of operation.
| Details | Details of the operation and, if available, further information in an expandable box.

The audit log shows the last 10 logs. In the action bar, you can change that number by setting a new limit and refreshing the audit log.

Additionally you can filter the entries by text or status by using the filter controls in the action bar.