---
title: Operating DataHub
weight: 50
layout: bundle
---

This section describes how you can examine the configuration and status of the system as well as the audit logs.

>**Info**: You need administration permissions to access system information and audit logs. See section [Defining DataHub permissions and roles](/datahub/setting-up-datahub#defining-permissions) for details.