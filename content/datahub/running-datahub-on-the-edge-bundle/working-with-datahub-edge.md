---
weight: 30
title: Working with DataHub Edge
layout: redirect
---

DataHub Edge offers the same set of functionality as the cloud variant. See section [Working with DataHub](/datahub/working-with-datahub) for details on configuring and monitoring offloading jobs, querying offloaded Cumulocity IoT data, and refining offloaded Cumulocity IoT data.

