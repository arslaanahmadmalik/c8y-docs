---
title: Running DataHub on the Edge
weight: 60
layout: bundle
---

This section describes how to run Cumulocity IoT DataHub on the Cumulocity IoT Edge, the local version of Cumulocity IoT.