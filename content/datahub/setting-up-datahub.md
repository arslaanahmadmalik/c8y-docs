---
title: Setting up DataHub
weight: 30
layout: bundle
---

This section describes how to set up Cumulocity IoT DataHub.