---
weight: 10
title: Overview
layout: redirect
---

REST is a very simple and secure protocol based on HTTP(S) and TCP. It is today the de-facto Internet standard supported by all networked programming environments ranging from very simple devices up to large-scale IT. One of the many books introducing REST is [RESTful Web Services](http://oreilly.com/catalog/9780596529260).

This section explains how to use Cumulocity IoT's REST interfaces to integrate devices with Cumulocity IoT. For general information on using REST interfaces and for information on developing applications on top of Cumulocity IoT using REST refer to the Microservice SDK guide.

The description is closely linked to the reference guide, which describes each interface in detail. Relevant chapters in the reference guide are in particular

-   [REST implementation](/reference/rest-implementation) is the reference for all general concepts.
-   [Device management library](/reference/device-management) specifies the data model for device management.
-   [Sensor library](/reference/sensor-library) specifies the data model for sensors and controls.

If you develop using Java ME/SE, JavaScript or C/C++, check the relevant chapters in this guide for even more convenient access to Cumulocity IoT's functionality. Also, if you use any of the supported development boards, see the corresponding description in the Devices guide for more information.
