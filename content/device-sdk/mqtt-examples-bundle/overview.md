---
weight: 1
title: Overview
layout: redirect
---

In this section, [Hello MQTT](#hello-mqtt) provides an easy introduction to the Cumulocity IoT MQTT protocol using a MQTT cross platform application.

It also contains examples on how to use the MQTT client with Cumulocity IoT employing pre-defined messages (called "static templates") using [C](#hello-mqtt-c), [Java](#hello-mqtt-java), [JavaScript](#hello-mqtt-javascript), [Python](#hello-mqtt-python) and [C#](#hello-mqtt-cs).

All the examples can be downloaded from the [GitHub repository](https://github.com/SoftwareAG/c8y_hw_mqtt).
