---
weight: 20
title: Versions
layout: redirect
---

Cumulocity IoT Edge “April 2020 release” uses the following versions:

|<div style="width:130px">Edge component</div>|<div style="width:300px">Version</div>|
|:---|:---|
|[Cumulocity IoT Core](/concepts)|10.6.0.2|
|[Apama](/apama)|10.6.0.2|

