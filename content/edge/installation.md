---
weight: 30
title: Setting up Cumulocity IoT Edge
layout: bundle
---

This document describes how to set up Cumulocity IoT Edge, the local version of Cumulocity IoT Core, in a Virtual Machine (VM).
