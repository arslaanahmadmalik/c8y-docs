---
weight: 50
title: Operating Cumulocity IoT Edge
layout: bundle
aliases:
  - /edge/appendix
  - /edge/password
---

This section describes the main operating procedures for standard tasks that have to be carried out when managing Cumulocity IoT Edge. 
