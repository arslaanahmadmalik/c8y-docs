---
weight: 50
title: Microservices
layout: redirect
---

Microservices are server-side applications which may be used to extend the Cumulocity IoT Edge platform with customer-specific functionality. For more information, see [Microservices SDK](/microservice-sdk/introduction/).

To enable or disable the microservice hosting feature, see [Enabling or Disabling microservice hosting feature](/edge/installation/#enabling-or-disabling-microservice-hosting-feature). The Device Simulator microservice is pre-installed with Edge VM and is enabled only if the microservice hosting feature is enabled. 