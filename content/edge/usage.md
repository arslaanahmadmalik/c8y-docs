---
weight: 40
title: Working with Cumulocity IoT Edge
layout: bundle
---

>**Important:** The tenant in the Edge instance is always named as **“edge“**.