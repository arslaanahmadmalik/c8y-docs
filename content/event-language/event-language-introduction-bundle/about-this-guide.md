---
weight: 10
title: About this guide
layout: redirect
---

<div style="padding: 24px ; border: 2px solid #1776BF; border-radius: 4px; margin-bottom: 24px; background-color: #f6fafe ">
  <h3 style="color: #1776BF"><b>IMPORTANT</b></h3> 
  <p class="lead"> The functionality described in this CEL analytics guide is deprecated. All new Cumulocity installations will use the Apama CEP engine. Using the Esper CEP engine is still supported for older installations but will no longer be provided for new installations and not be invested into in the future. </p>

  <p><b>For further information on using Apama's Event Processing Language in Cumulocity refer to the <a href="/apama/introduction">Streaming Analytics guide</a>.</b></p>
  
For details on migration, refer to <a href="/apama/overview-analytics/#migrate-from-esper">Migrating from CEL (Esper) to Apama</a> in the Streaming analytics guide.

</div>

The CEL analytics guide consists of the following sections:

* Introduction
* [Data model](/event-language/data-model)
* [Functions](/event-language/functions)
* [Advanced uses cases](/event-language/advanced-cel)
* [Best practices and troubleshooting](/event-language/best-practices-cel)
* [Examples](/event-language/examples)
* [Study: Circular geofence alarms](/event-language/geofence)
