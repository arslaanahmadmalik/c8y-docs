---
weight: 30
title: Functions
layout: bundle
---

With the Cumulocity Event Language it is possible to utilize functions. This section will cover the already built-in functions ready to use. 

For guidance on how to write your own functions please check [Creating own functions](/event-language/advanced#creating-own-functions).