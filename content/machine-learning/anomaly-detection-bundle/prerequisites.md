---
title: Prerequisites
layout: redirect
weight: 20

aliases:
  - /predictive-analytics/anomaly-detection/#prerequisites
---

[Download the AnomalyDetectionDemo.zip](/files/zementis/AnomalyDetectionDemo.zip) file which contains demo scripts, training data set, PMML Model and EPL rule.

Running the demo scripts requires 

* Prior experience with Python, JSON, REST and understanding of data science processes.
* Familiarity with Cumulocity IoT and its in-built apps.
* Subscription of the Zementis microservice (10.5.0.x.x or higher) and the Machine Learning application on the tenant.
* Subscription of the Apama-ctrl microservice and the Apama-epl application on the tenant.