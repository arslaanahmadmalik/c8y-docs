---
title: Prerequisites
layout: redirect
weight: 20

aliases:
  - /predictive-analytics/demand-forecasting/#prerequisites
---

[Download the DemandForecastingDemo.zip](/files/zementis/DemandForecastingDemo.zip) file which contains the scripts and sample data set used in this demo.

Running the demo scripts requires 

* Prior experience with Python, Jupyter Notebook, JSON, REST and understanding of data science processes.
* Familiarity with Cumulocity IoT and its in-built apps.
* Subscription of the Zementis microservice (10.6.0.x.x or higher) and Nyoka microservice (10.6.0.x.x or higher) on the tenant.
* Installation of Jupyter Notebook using Python 3.x environment with the following packages installed:
  - requests
  - jsonpath_ng 
  - pandas 
  - matplotlib