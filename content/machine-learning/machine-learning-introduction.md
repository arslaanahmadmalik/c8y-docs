---
weight: 10
title: Introduction
layout: bundle
slug: introduction

aliases:
  - /predictive-analytics/introduction
---

The machine learning capabilities in the Cumulocity IoT platform enable you to deploy and manage machine learning models and custom resources, and use them for generating predictions on data gathered from your devices.

These capabilities can be leveraged either from a web browser via an easy to use UI (Machine Learning application) or programmatically via REST API (Zementis microservice). 

This guide provides detailed information on the functionalities of the [Machine Learning application](/machine-learning/web-app) and a comprehensive listing of [REST API](/machine-learning/api-reference) exposed by the Zementis microservice.