---
title: Managing resources
layout: redirect
weight: 40

aliases:
  - /predictive-analytics/web-app/#managing-resources
---

In the **Resources** page you manage the resources, i.e. the custom functions and look-up tables which a model might need.

Resource management functionality includes:

* Uploading resources
* Downloading resources
* Deleting resources

Click **Resources** in the navigator, to open the **Resources** page. 

![Resources](/images/zementis/zementis-resources.png)

>**Info**: Currently, resource management is only applicable for resources associated to PMML models.

### Uploading resources

To upload a new resource, click **Add resource**, navigate to the desired resource file and then click **Open**. 

Once your resource is successfully uploaded, you will see a corresponding confirmation message. The new resource will be added to the resources list. 


### Downloading resources

To download the source file of a resource, click the download icon in its card.

Typically the source of the resource will either be a jar file or an Excel sheet.

### Deleting resources

To delete a resource, click the delete icon on its card and confirm the deletion.  

Once a resource is deleted, it will be removed permanently from your resources list.