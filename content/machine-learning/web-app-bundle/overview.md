---
title: Overview
layout: redirect
weight: 10

aliases:
  - /predictive-analytics/web-app/#overview
---

The Machine Learning application enables you to manage your models by providing options for uploading, downloading, or activating/deactivating your models. Additionally, it also provides you with an insight into your models by capturing runtime performance and showcasing it via meaningful KPIs.

Moreover, it enables you to manage custom resources which your models might need. These resources include custom functions and look-up tables.

The following sections will walk you through all functionalities of the Machine Learning application in detail.
 
