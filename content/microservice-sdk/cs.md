---
weight: 50
layout: bundle
title: Microservice SDK for C#
aliases:
  - /cs
  - /cs/introduction
  - /cs/developing-microservice
  - /cs/hello-world-basic
---

This section describes how to develop and deploy microservices on top of Cumulocity using the Microservice SDK for C#.
