---
weight: 10
title: Overview
layout: subsections
collection: 'microservice-sdk/http'
---

This section contains step-by-step tutorials to successfully develop microservices which employ the Cumulocity IoT APIs and other third-party services. The source code of the examples can be found in our [Bitbucket M2M repository](https://bitbucket.org/m2m/cumulocity-examples/src/default/microservices).

On the Cumulocity IoT platform, microservice hosting is built on top of Docker containers. This makes it technology-agnostic and allows developers to create applications in any technology stack.
