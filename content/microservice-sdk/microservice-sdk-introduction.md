---
weight: 10
title: Overview
layout: bundle
slug: introduction
---

Microservices are server-side applications which may be used to extend the Cumulocity platform with customer-specific functionality.

This Microservice SDK guide provides detailed information on:

* [General concepts](/microservice-sdk/concept) and functionality of using microservices on top of Cumulocity
* How to develop and deploy microservices using the SDK for [C#](/microservice-sdk/cs) and [Java](/microservice-sdk/java)
* How to use the [Cumulocity's REST interfaces](/microservice-sdk/rest) to develop microservices
* Examples on developing microservices using [Python](/microservice-sdk/http#hello-microservice-python), [Java](/microservice-sdk/http#microservice-java) and [Node.js](/microservice-sdk/http#microservice-nodejs)
