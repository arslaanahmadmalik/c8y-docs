---
weight: 6
title: Application names
layout: redirect
---

For each tenant, Cumulocity IoT manages the subscribed applications. Cumulocity IoT provides a number of applications of various types.

In case you want to subscribe a tenant to an application using an API, you need to use the application name in the argument (as name).

Refer to the tables in [Administration > Managing applications](/users-guide/administration#managing-applications) in the User guide for the respective application name to be used.
