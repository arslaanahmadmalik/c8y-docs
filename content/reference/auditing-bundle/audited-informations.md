---
weight: 10
title: Audited informations
layout: redirect
---

-   Alarm modifications
-   Operation modifications
-   Two factor authentication login attempts
-   Smart rule modifications
-   Complex Event Processing module modifications
-   User and group permissions modifications
