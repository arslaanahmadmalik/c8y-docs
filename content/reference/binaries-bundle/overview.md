---
weight: 5
title: Overview
layout: redirect
---
The binaries interface consists of the following parts:

-   The *binaries collection* resource retrieves sets with information about uploaded binaries and enables uploading new binaries.
-   The *binaries* resource represents binaries that can be downloaded, updated or deleted.

> Note that for all PUT/POST requests accept header should be provided, otherwise an empty response body will be returned.