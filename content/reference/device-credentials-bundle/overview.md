---
weight: 5
title: Overview
layout: redirect
---
The device credentials interface consists of the following parts:

* Device requests are used to register new devices with a tenant.
* Device credentials provide credentials to registered devices.
* Device credentials provide endpoint to bulk credentials provision.

For more information on the process of registering devices, see [Device integration](/device-sdk/rest#device-integration) in the Device SDK guide.

> Note that for all PUT/POST requests accept header should be provided, otherwise an empty response body will be returned.
