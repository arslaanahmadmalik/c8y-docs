---
weight: 10
title: Overview
layout: redirect
---

The device management library defines the data structures that are used in Cumulocity IoT for device management activities such as software management and configuration management.

The data structures are expressed as fragments that can be used inside managed objects, operations and other resources. More information on the fragment concept can be found in the Section "[Cumulocity IoT's domain model](/concepts/domain-model)". The same section also contains information on the process of running operations on devices and updating the inventory according to the result of the operation. For the usage of fragments in client libraries, see the developer's guides for the respective client library.
