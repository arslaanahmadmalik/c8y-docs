---
weight: 60
title: Other response formats
layout: redirect
---
In order to get measurements in other formats than JSON you can use one of the following supported `Accept` headers:

- `text/csv`
- `application/vnd.ms-excel`

