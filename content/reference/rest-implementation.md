---
weight: 10
title: REST implementation
layout: bundle
aliases:
  - /reference-guide/rest-implementation
  - /reference-guide
---

This section describes the aspects common to all REST-based interfaces of Cumulocity IoT. The interfaces are based on the [Hypertext Transfer Protocol 1.1](https://tools.ietf.org/html/rfc2616) using [HTTPS](http://en.wikipedia.org/wiki/HTTP_Secure).
