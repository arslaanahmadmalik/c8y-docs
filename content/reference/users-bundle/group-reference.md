---
weight: 90
title: Group reference
layout: redirect
---

### GroupReference [application/vnd.com.nsn.cumulocity.groupReference+json]

|Name|Type|Occurs|Description|
|:---|:---|:-----|:----------|
|self|URI|1|Link to this resource|
|group|Group|1|Group resource being referenced|
