---
weight: 100
title: Role collection
layout: redirect
---

### RoleCollection [application/vnd.com.nsn.cumulocity.roleCollection+json]

|Field Name|Type|Occurs|Description|
|:---------|:---|:-----|:----------|
|self|URI|1|Link to this resource|
|roles|Role|0..n|List of roles|
|statistics|PagingStatistics|1|Information about the paging statistics|
|prev|URI|0..1|Link to a possible previous page with additional roles|
|next|URI|0..1|Link to a possible next page with additional roles|
