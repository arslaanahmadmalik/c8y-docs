---
weight: 130
title: Role reference
layout: redirect
---

### RoleReference [application/vnd.com.nsn.cumulocity.roleReference+json]

|Name|Type|Occurs|Description|
|:---|:---|:-----|:----------|
|self|URI|1|Link to this resource|
|role|Role|1|A role resource being referenced|
