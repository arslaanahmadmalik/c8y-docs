---
weight: 50
title: User reference
layout: redirect
---

### UserReference [application/vnd.com.nsn.cumulocity.userReference+json]

|Name|Type|Occurs|Description|
|:---|:---|:-----|:----------|
|self|URI|1|Link to this resource|
|user|User|1|User resource being referenced|
