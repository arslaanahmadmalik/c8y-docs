---
weight: 30
title: Documentation changes
layout: redirect
---

In addition to all enhancements or changes related to the improvements and fixes of the 10.5.0 release, the following improvements or modifications have been made in the area of documentation.

### Installation and operations guides

The Installation and operation guides for the Cumulocity IoT platform are now available through the [Software AG Empower Portal](https://empower.softwareag.com) (credentials required).

### General: About section

A new [About](/about-doc/intro-documentation/) section has been added to the documentation, showing information on documentation versions, content, conventions and copyrights.

### User guide: Managing applications

The documentation of the microservice logging feature has been improved, see [Administration > Managing Applications](https://cumulocity.com/users-guide/administration/#managing-applications).


### Device guides: New device catalogue

A new [Cumulocity IoT Device Partner Portal](https://devicepartnerportal.softwareag.com/web/#/) has been implemented, offering an extensive [collection of devices](https://devicepartnerportal.softwareag.com/web/#/devices) with guaranteed plug-and-play compatibility and full functional support in the Cumulocity IoT platform. Apart from general information on each device you can find quick start guides here, describing how to configure the particular device and connect it to the Cumulocity IoT platform. 

The information formerly provided in the Device guides on the [Cumulocity IoT documentation website](https://www.softwareag.cloud/site/dev-center/cumulocity-iot.html#/) has been moved to the Partner Portal. The former Device guides are no longer available.  

#### Demo devices

The descriptions on how to integrate the demo devices **Cinterion Java modules**, **Mbed u-blox C027**, **Raspberry Pi**, and **Tinkerforge** with Cumulocity IoT have been moved from the Device guides to the examples repository in Bitbucket at [https://bitbucket.org/m2m/cumulocity-examples/src/develop/device-integrations]( https://bitbucket.org/m2m/cumulocity-examples/src/develop/device-integrations).

#### OPCUA agent 1.0

The documentation of the OPCUA Java gateway has been moved from the Device guides to the Java agent sources in Bitbucket at [https://bitbucket.org/m2m/cumulocity-agents-opc/src/develop/opcua-agent/documentation/](https://bitbucket.org/m2m/cumulocity-agents-opc/src/develop/opcua-agent/documentation/).

### Device SDK guide: Device SDK for Java

The *Device SDK for Java* documentation has been moved from the [Device SDK guide](/guides/device-sdk/introduction/) to the Java Device SDK sources in Bitbucket at [https://bitbucket.org/m2m/cumulocity-clients-java/src/develop/device-sdk-documentation/](https://bitbucket.org/m2m/cumulocity-clients-java/src/develop/device-sdk-documentation/).

### Device SDK guide: Device SDK for C &#35;

The *Device SDK for C#* documentation has been moved from the [Device SDK guide](/guides/device-sdk/introduction/) to the C# Device SDK sources in Bitbucket at h[ttps://bitbucket.org/m2m/cumulocity-clients-cs/src/develop/DeviceSDK/Documentation/](https://bitbucket.org/m2m/cumulocity-clients-cs/src/develop/DeviceSDK/Documentation/). 

### Analytics guide

The Analytics guide has been renamed to Streaming Analytics guide (as opposed to  Predictive analytics guide).

### DataHub guide

A new [Datahub guide](/guides/datahub/datahub-overview) has been added to the documentation, describing how to perform sophisticated analytical querying over device data with Cumulocity IoT DataHub, which is now available in the Cumulocity IoT Cloud platform as an integrated web application.
