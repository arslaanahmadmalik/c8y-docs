---
weight: 21
title: Documentation changes
layout: redirect
---

In addition to all enhancements or changes related to the improvements and fixes of the 10.5.7 release, the following improvements or modifications have been made in the area of documentation.

### Device integration tutorials

In addition to the device guides offered in the [Cumulocity IoT Device Partner Portal](https://devicepartnerportal.softwareag.com/web/#/), we have re-written and added a collection of device integration tutorials which describe in a step-by-step approach how to integrate various demo devices to Cumulocity IoT, see [Device integration tutorials](/device-tutorials/tutorials-introduction).

Moreover, documentation on how to set up and configure a Casa Systems (NetComm) router using the new [Cumulocity IoT NetComm Agent](/device-tutorials/netcomm-routerhugo server) package has been added here.

### Installation and operations guides

The Installation and operation guides for Cumulocity IoT Core now include all relevant information for the installation and operation of Apama components. In addition, the following guides are now available through the [Software AG Empower Portal](https://documentation.softwareag.com/):

* **LWM2M** - Installation & operations guide
* **SSL Management** - Installation & operations guide
* **Zementis** microservice - Installation & operations guide

Note, that access to these documents on the Empower Portal requires credentials.

### Machine learning guide

The former Predictive Analytics offering has been rebranded to **[Cumulocity IoT Machine Learning](/machine-learning/)**. Accordingly, the name of the respective guide has been changed.

### Microservice SDK guide

A new [Best Practices collection](/microservice-sdk/microservices-best-practices/) has been added to the Microservice SDK guide, providing recommendations to be considered before developing and deploying microservices on top of Cumulocity IoT. 
