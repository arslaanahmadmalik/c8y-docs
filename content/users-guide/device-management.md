---
weight: 20
title: Device Management
layout: bundle
---

The Device Management application provides functionalities for managing and monitoring devices and enables you to control and troubleshoot devices remotely. 