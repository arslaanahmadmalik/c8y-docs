---
weight: 50
title: Enterprise Tenant
layout: bundle
---


The Enterprise Tenant of the Cumulocity IoT platform provides several enhancements to the features for the Standard Tenant. The following sections describe additional functionalities available in the Enterprise Tenant.