---
weight: 30
title: Cumulocity IoT applications
layout: redirect
aliases:
  - /users-guide/overview/#cumulocity-applications
---

Per default, Cumulocity IoT comes with the following three standard applications:

<table>
<col width = 100>
<col width = 150>
<thead>
<tr>
<th style="text-align:center">&nbsp;</th>
<th style="text-align:left">Application</th>
<th style="text-align:left">Description</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center"><i class="c8y-icon c8y-icon-administration c8y-icon-duocolor" style="font-size: 36px;"></i></td>
<td style="text-align:left"><a href="/users-guide/administration" class="no-ajaxy">Administration</a></td>
<td style="text-align:left">The Administration application enables account administrators to manage their users, roles, tenants, applications and business rules and lets them configure a number of settings for their account. </td>
</tr>
<tr>
<td style="text-align:center"><i class="c8y-icon c8y-icon-cockpit c8y-icon-duocolor" style="font-size: 36px;"></i></td>
<td style="text-align:left"><a href="/users-guide/cockpit" class="no-ajaxy">Cockpit</a></td>
<td style="text-align:left">The Cockpit application provides you with options to manage and monitor  Internet of Things (IoT) assets and data from a business perspective.</td>
</tr>
<tr>
<td style="text-align:center"><i class="c8y-icon c8y-icon-device-management c8y-icon-duocolor" style="font-size: 36px;"></i></td>
<td style="text-align:left"><a href="/users-guide/device-management" class="no-ajaxy">Device Management</a></td>
<td style="text-align:left">The Device Management application provides functionalities for managing and monitoring devices and enables you to control and troubleshoot devices remotely.  </td>
</tr>

</tbody>
</table>

Apart from these standard applications that initially come with Cumulocity IoT, various additional applications are provided, offering a wide variety of functionalities. The availability of this applications depends on the Tenant you are using and the applications your organization is subscribed to.

Find a detailed list of all applications available with Cumulocity IoT in  [Administration > Managing applications](/users-guide/administration/#managing-applications).

On top of this, the Cumulocity IoT platform provides broad functionality to add your own applications to be used in your Cumulocity IOT account, see [Administration > Managing applications](/users-guide/administration#managing-applications) for details.

The content of the Cumulocity IoT platform therefore is entirely dynamic and is generated based on various criteria:

* The applications that your organization has subscribed to.
* The applications that your organization has configured or developed itself for Cumulocity IoT.
* The functionality that your user is permitted to see.
* The configuration of the user interface, such as groups and reports.
