---
title: Getting Started
weight: 10
layout: bundle
aliases:
  - /users-guide/overview
---

The Getting Started section describes how to access and log into the Cumulocity IoT platform and will walk you through basic functionalities applying to all Cumulocity IoT applications.
