---
weight: 90
layout: redirect
title: webMethods.io Integration
---

webMethods.io Integration is Software AG's cloud based integration solution. It enables you to automate tasks by connecting cloud applications and services (such as Marketo, Salesforce, Evernote, and Gmail) without writing any code.

The complete webMethods.io Integration documentation is available at [https://docs.webmethods.io/](https://docs.webmethods.io/).

### Getting started

>**Info:** If your tenant has been created outside of SAG Cloud you will not benefit from the user experience described below. You can still use webMethods.io to integrate Cumulocity IoT with other applications, but you cannot use the app switcher and single sign-on login.

To subscribe to webMethods.io Integration, perform the following steps:

1. Log into the Cumulocity IoT platform as part of **Software AG Cloud**.

2. In the application switcher, select **webMethods.io Integration**.

![webMethods.io App Switcher Integration](/images/users-guide/webMethods.io/wmio-appswitcher-integration.png)

>**Info:** If the icon is unavailable you might not be subscribed to **webMethods.io Integration**. Subscribe to it by opening the application switcher and clicking **MyCloud**. This will take you to the **Software AG Cloud** portal where you can subscribe for a free trial.
<br><br>![webMethods.io App Switcher My Cloud](/images/users-guide/webMethods.io/wmio-appswitcher-mycloud.png)

### Examples

Integrations in webmethods.io are called "workflows". A workflow is a connection between two or more web apps or services. It’s like a set of steps required to perform a task.

The example workflow below is triggered by an alarm in Cumulocity IoT and creates a ticket in Zendesk and sends an SMS message.

![webMethods.io Example Workflow](/images/users-guide/webMethods.io/wmio-example1.png)

WebMethods.io also provides pre-configured workflows which are called "recipes".

![webMethods.io Example Recipe](/images/users-guide/webMethods.io/wmio-recipe-salesforce.png)

More examples and technical guides can be found on the Software AG TECHcommunity website [https://techcommunity.softwareag.com/](https://techcommunity.softwareag.com/) in the **Tips, Tricks and Code** section.
