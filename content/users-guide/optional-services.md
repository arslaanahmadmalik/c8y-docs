---
weight: 60
title: Optional services
layout: bundle
aliases:
  - /users-guide/tenant-sla-monitoring
  - /users-guide/LoRa
  - /users-guide/jasper
  - /users-guide/android-cloud-sensor-app
  - /users-guide/impact
  - /users-guide/saas-integration
  - /users-guide/cloud-fieldbus
  - /users-guide/cloud-remote-access
  - /users-guide/zapier
  - /users-guide/data-broker
  - /users-guide/IMPACT
  - /devices/netcomm/cloud-fieldbus
  - /devices/sigfox
  - /benutzerhandbuch/cloud-fieldbus-deutsch
  - /benutzerhandbuch/jasper-deutsch
  - /benutzerhandbuch/saas-integration-deutsch
  - /benutzerhandbuch/android-cloud-remote-access-deutsch
---
In addition to the standard and built-in applications that come with Cumulocity IoT, various additional applications are provided which you may subscribe to, i.e. integrated agents for several device types.
