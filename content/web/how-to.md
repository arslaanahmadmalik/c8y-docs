---
weight: 70
title: How-to recipes
layout: bundle
alias: 
 - /web/troubleshooting-bundle/3rdparty-library.md
 - /web/troubleshooting-bundle/dont-run.md
 - /web/troubleshooting-bundle/error-in-tenant.md
 - /web/troubleshooting.md
---

This section lists common how-to recipes for [Web SDK for Angular](/web/angular). They require:
 
 - a basic understanding of Angular components, services and modules.
 - an understanding on how to scaffold an application and how to run it with the [@c8y/cli](/web/angular/#cli).
 - a basic understanding of the extension points concepts of the [@c8y/ngx-components](/web/angular/#extension-points).

 > **Info:** All recipes are written with a particular version of Web SDK for Angular. The version is mentioned at the top of the recipe. It is not recommended to use an older version for a recipe, as some of the mentioned features might not be available. If you use a newer version, you might face naming or import changes. We will update the recipes if there are conceptual revisions but not for small variations. Check out the tutorial application with `c8ycli new my-app tutorial` to have an up-to-date example of all concepts.