---
weight: 50
title: Web SDK for plugins
layout: bundle
aliases:
  - /http
  - /web/jsdoc
  - /web/branding-plugin
  - /web/tab-plugin
  - /web/widget-plugin
---

The [Web SDK for plugins](/web/web-sdk-for-plugins) is deprecated. To develop new web applications we recommend using [Web SDK for Angular](/web/angular). If you have already developed a plugin, refer to [Web SDK for Angular > Migrating](/web/angular#migrating) for information on how to import plugins into the Web SDK for Angular.