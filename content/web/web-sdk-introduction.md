---
weight: 10
title: Overview
layout: bundle
slug: introduction
aliases:
  - /developers-guide/developing-web-clients
  - /developers-guide/developing-web-clients.html
  - /web/smart-toolkit
  - /web/reference
---

This Web SDK guide provides information on the Web SDK which enables you to develop web applications on top of our platform, communicate through our API and apply UI components to your custom application.

The latest generation of our Web SDK - [Web SDK for Angular](/web/angular) - has now been released. 

We strongly recommend using [Web SDK for Angular](/web/angular) when developing new web applications.

Web SDK for Angular JS is deprecated. Therefore, its documentation is no longer provided here.  As all Cumulocity IoT REST APIs are backward compatible, Angular JS applications will still continue to work. 

[Web SDK for plugins](/web/web-sdk-for-plugins) is based on Angular JS and is also deprecated. For Angular-based development, we recommend to implement native Angular modules. The documentation is still available for migration purposes, i.e. if you want to integrate an already developed plugin into the [Web SDK for Angular](/web/angular). You can import plugins to Angular as described in [ngx-components > Extension points](/web/angular#extension-points).

If you have previously been working with older versions you might be interested in some short information on the evolution of the UI stack in [Upgrading to Angular](/web/background) to better understand how and why it is now designed the way it is.